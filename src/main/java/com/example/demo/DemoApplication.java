package com.example.demo;

import com.example.demo.components.Student;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context=	SpringApplication.run(DemoApplication.class, args);
		Student studentRef=context.getBean(Student.class);
		studentRef.display();
	}

}
